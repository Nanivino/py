# py

Graph Plotting in Python | Set 1
Difficulty Level : Easy
Last Updated : 19 Oct, 2021
This series will introduce you to graphing in python with Matplotlib, which is arguably the most popular graphing and data visualization library for Python.
Installation
The easiest way to install matplotlib is to use pip. Type following command in terminal: 
 

pip install matplotlib
OR, you can download it from here and install it manually. 
 

Getting started ( Plotting a line)


# importing the required module
import matplotlib.pyplot as plt
 
# x axis values
x = [1,2,3]
# corresponding y axis values
y = [2,4,1]
 
# plotting the points
plt.plot(x, y)
 
# naming the x axis
plt.xlabel('x - axis')
# naming the y axis
plt.ylabel('y - axis')
 
# giving a title to my graph
plt.title('My first graph!')
 
# function to show the plot
plt.show()
Output: 
 

mp1

The code seems self-explanatory. Following steps were followed: 

Define the x-axis and corresponding y-axis values as lists.
Plot them on canvas using .plot() function.
Give a name to x-axis and y-axis using .xlabel() and .ylabel() functions.
Give a title to your plot using .title() function.
Finally, to view your plot, we use .show() function.
 

Plotting two or more lines on same plot


import matplotlib.pyplot as plt
 
# line 1 points
x1 = [1,2,3]
y1 = [2,4,1]
# plotting the line 1 points
plt.plot(x1, y1, label = "line 1")
 
# line 2 points
x2 = [1,2,3]
y2 = [4,1,3]
# plotting the line 2 points
plt.plot(x2, y2, label = "line 2")
 
# naming the x axis
plt.xlabel('x - axis')
# naming the y axis
plt.ylabel('y - axis')
# giving a title to my graph
plt.title('Two lines on same graph!')
 
# show a legend on the plot
plt.legend()
 
# function to show the plot
plt.show()
Output: 
 

mp2

 

Here, we plot two lines on the same graph. We differentiate between them by giving them a name(label) which is passed as an argument of the .plot() function.
The small rectangular box giving information about the type of line and its color is called a legend. We can add a legend to our plot using .legend() function.
 

Customization of Plots

Here, we discuss some elementary customizations applicable to almost any plot.


import matplotlib.pyplot as plt
 
# x axis values
x = [1,2,3,4,5,6]
# corresponding y axis values
y = [2,4,1,5,2,6]
 
# plotting the points
plt.plot(x, y, color='green', linestyle='dashed', linewidth = 3,
         marker='o', markerfacecolor='blue', markersize=12)
 
# setting x and y axis range
plt.ylim(1,8)
plt.xlim(1,8)
 
# naming the x axis
plt.xlabel('x - axis')
# naming the y axis
plt.ylabel('y - axis')
 
# giving a title to my graph
plt.title('Some cool customizations!')
 
# function to show the plot
plt.show()
Output: 
 

mp3

As you can see, we have done several customizations like 

setting the line-width, line-style, line-color.
setting the marker, marker’s face color, marker’s size.
overriding the x and y-axis range. If overriding is not done, pyplot module uses the auto-scale feature to set the axis range and scale.
  
 

Bar Chart


import matplotlib.pyplot as plt
 
# x-coordinates of left sides of bars
left = [1, 2, 3, 4, 5]
 
# heights of bars
height = [10, 24, 36, 40, 5]
 
# labels for bars
tick_label = ['one', 'two', 'three', 'four', 'five']
 